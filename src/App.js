import './App.css';
import AppNavBar from './components/AppNavBar';
import { Container } from 'react-bootstrap';
import Home from './pages/Home';
import Products from './pages/Products';
import Register from './pages/Register';
import Login from './pages/Login';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Logout from './pages/Logout';
import ErrorPage from './pages/ErrorPage';
import { useState, useEffect } from 'react';
import { UserProvider } from './userContext';
import ProductView from './pages/ProductView';
import AdminDashboard from './pages/AdminDashboard';
import CreateProduct from './pages/CreateProduct';
import UpdateProduct from './pages/UpdateProduct';
import ArchiveProduct from './pages/ArchiveProduct';
import ActivateProduct from './pages/ActivateProduct';
import Slideshow from './components/Slideshow';
import ProductReviews from './pages/ProductReviews';
import ContactUs from './pages/ContactUs';

function App() {

  const [user, setUser] = useState({
      id: null,
      isAdmin: null
  });

  const unsetUser = () => {
    
    localStorage.clear();
  }

  useEffect(() => {
    console.log(user)
    console.log(localStorage)
  })

  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <AppNavBar />
          <Container>
              <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/reviews" element={<ProductReviews />} />
                <Route path="/products" element={<Products />} />
                <Route path="/register" element={<Register />} />
                <Route path="/login" element={<Login />} />
                <Route path="/logout" element={<Logout />} />
                <Route path="/contactus" element={<ContactUs />} />
                <Route path="/productView/:productId" element={<ProductView />} />
                <Route path="/admindashboard" element={<AdminDashboard />} />
                <Route path="/newproduct" element={<CreateProduct />} />
                <Route path="/update/:productId" element={<UpdateProduct />} />
                <Route path="/archive/:productId" element={<ArchiveProduct />} />
                <Route path="/activate/:productId" element={<ActivateProduct />} />
                <Route path="*" element={<ErrorPage />}/>
              </Routes>
          </Container>
      </Router>
    </UserProvider>
  );
}

export default App;

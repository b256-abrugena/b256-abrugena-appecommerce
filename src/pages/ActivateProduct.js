import { useState, useEffect, useContext } from 'react';
import { Form, Button, Container, Card, Row, Col } from 'react-bootstrap';
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function ActivateProduct() {

	const{ user, setUser } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})
	})

	const reactivateProduct = () => {

		fetch(`${process.env.REACT_APP_API_URL}/products/activate/${productId}`, {
			method: "PATCH",
			headers:{
				'Content-Type': 'application/json',
				Authorization : `Bearer ${localStorage.getItem('token')}`,
			},
			body: JSON.stringify({
				isActive: true
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data !== false){

				Swal.fire({
					title: "Success!",
					icon: "success",
					text: "The product has been activated!"
				})

				navigate("/admindashboard");

			}
			else{
				Swal.fire({
					title: "Error",
					icon: "error",
					text: "Product was not activated."
				})
			}

		})
	}

	

	return(
		<>
			<h1 className="text-center mt-5 mb-5 pt-5 text-light display-2">Activate Product</h1>
			<p className="h2 text-light text-center pb-3">By clicking the "Activate" button, you will activate this product:</p>
			<Container className="mt-5">
			<Row>
				<Col lg={{ span: 6, offset: 3 }}>
					<Card className="bg-dark pcard p-3 mb-3 d-md-flex justify-content-around">
						<Card.Body className="text-center">
							<Card.Title className="text-center bg-light text-dark pt-1 pb-1">{name}</Card.Title>
							<Card.Subtitle className="text-light mt-4">Description:</Card.Subtitle>
							<Card.Text className="text-light mb-4">{description}</Card.Text>
							<Card.Subtitle className="text-light">Price:</Card.Subtitle>
							<Card.Text className="text-light">₱ {price}</Card.Text>
							<Button variant="primary" block onClick={() => reactivateProduct()}>Activate</Button>	
						</Card.Body>		
					</Card>
				</Col>
			</Row>
			</Container>
		</>	
	)
}
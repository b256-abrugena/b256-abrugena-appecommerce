import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { useState, useEffect, useContext } from 'react'; 
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import img41 from '../images/img41.jpg'
import img42 from '../images/img42.jpg'
import img43 from '../images/img43.jpg'

export default function ProductReviews() {
    const data = {
        title: "Product Reviews",
        content: "Why you should choose Sky Perfumes? Let us take a look at the feedbacks below from our valued customers!",
        destination: "/products",
        label: "Back to Products"
    }

    return (
        <>
            <Banner data={data}/>
            <div className="container-fluid pt-4 pb-4 align-middle">
	            <div className="row">
	                <div className="caption col-sm-6 order-2 order-md-1 p-5 align-middle">
	                	<h3 className="text-center pt-3">⭐⭐⭐⭐⭐</h3>
	                    <h4 className="text-center pt-3 pb-3 display-5">Excellent Quality! 👌</h4>
	                    <p className="p-2 text-center">SKY PERFUMES never fails me in providing the products that I need in order for me to stay fragrant! From the packaging, delivery and legitimacy of their product, I am confident that their products are authentic! In addition, compared to other perfume stores, they offer the perfumes at lowest price! So what are you waiting for?? Buy now at SKY PERFUMES! Kudos to you, Kang Haneul!! </p>
	                    <p className="mt-4 pt-4 text-center"><em>-- Park Seo Joon (박서준) --</em></p>
	                    <p className="text-center"><em>Actor, South Korea</em></p>
	                    <p className="text-center"><em>(Product: Bleu de Chanel)</em></p>
	                </div>

	                <div className="col-sm-6 order-1 order-md-2">
	                    <img src={img41} className="img-fluid pl-2 pr-2"></img>
	                </div>
	            </div>
        	</div>
        	<div className="container-fluid pt-4 pb-4 align-middle">
        		<div className="row">
	                <div className="col-sm-6">
	                    <img src={img42} className="img-fluid pl-2 pr-2"></img>
	                </div>

	                <div className="caption col-sm-6 p-5 align-middle">
	                	<h3 className="text-center pt-3">⭐⭐⭐⭐⭐</h3>
	                    <h4 className="text-center pt-3 pb-3 display-5">Fast and Easy Transaction! 👏👏👏</h4>
	                    <p className="p-2 text-center">I never thought that SKY PERFUMES will do an amazing job in selling such amazing perfumes! At first, I've been waiting several weeks in order to purchase and claim my signature scent due to lack of availability in other local stores. But now, I won't worry anymore because of this online shop! In addition, the seller is very responsive and accommodating when it comes to answering my inquiries! Congratulations Kang Haneul! Wishing the best of luck in growing your business! </p>
	                    <p className="mt-4 pt-4 text-center"><em>-- Hyun Bin (현빈) --</em></p>
	                    <p className="text-center"><em>Actor, South Korea</em></p>
	                    <p className="text-center"><em>(Product: Tom Ford Soleil Brûlant)</em></p>
	                </div>
	            </div>
        	</div>
        	<div className="container-fluid pt-4 pb-4 align-middle">
	            <div className="row">
	                <div className="caption col-sm-6 order-2 order-md-1 p-5 align-middle">
	                	<h3 className="text-center pt-3">⭐⭐⭐⭐⭐</h3>
	                    <h4 className="text-center pt-3 pb-3 display-5">Accommodating Seller! 🙌</h4>
	                    <p className="p-2 text-center">I truly believe that in order to grow your business successfully, you should have a good attitude when it comes to responding to the needs of your customers, and to update them of your products and services. SKY PERFUMES welcomes me with a good vibes, and never fails to update me when I'm waiting for the arrival of my product! They even ask me if there are other products that I prefer so that they can provide it beforehand. Kamsahamnida, Kang Haneul hyungnim! Hope this review will help in reaching more customers! </p>
	                    <p className="mt-4 pt-4 text-center"><em>-- Kim Myung Soo (김명수) --</em></p>
	                    <p className="text-center"><em>Actor, South Korea</em></p>
	                    <p className="text-center"><em>(Product: Jo Malone Wood Sage & Sea Salt Cologne)</em></p>
	                </div>

	                <div className="col-sm-6 order-1 order-md-2">
	                    <img src={img43} className="img-fluid pl-2 pr-2"></img>
	                </div>
	            </div>
        	</div>
        </>
    )
}
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function CreateProduct(){

	const{ user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(name);
	console.log(description);
	console.log(price);

	useEffect (() => {
		if(name !== '' && description !== ''  && price !==''){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}) 


	function createProduct(e){

		e.preventDefault();

		{
			(user.isAdmin) ?
			fetch(`${process.env.REACT_APP_API_URL}/products/create`, {
				method: "POST",
				headers:{
					'Content-Type': 'application/json',
					Authorization : `Bearer ${localStorage.getItem('token')}`
				},
				body: JSON.stringify({
					name: name,
					description: description,
					price: price,
					isAdmin: user.isAdmin
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				if(data !== false){

					Swal.fire({
						title: "Success!",
						icon: "success",
						text: "The product has been successfully added to the dashboard!"
					})

					navigate("/admindashboard");

				}
				else{
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Check the details of the product and try again."
					})
				}

			})
			:
			Swal.fire({
				title: "Log-in Error",
				icon: "error",
				text: "Admin is not recognized."
			})

			navigate("/login");
		}
		

		setName('');
		setDescription('');
		setPrice('');
	}

	return(
		<>
		<h1 className="text-center mb-5 mt-5 pt-5 text-light display-2">Create New Product</h1>
		<p className="h2 text-light text-center pb-3">Specify the details of the product that you want to create:</p>

		<Form onSubmit={e => createProduct(e)} className="bg-dark text-light text-center m-3 p-3">
		  <Form.Group className="mb-4" controlId="formBasicName">
	        <Form.Label>Product Name:</Form.Label>
	        <Form.Control type="text" placeholder="Enter the product name" value={name} onChange={e => setName(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-4" controlId="formBasicDescription">
	        <Form.Label>Description:</Form.Label>
	        <Form.Control type="text" placeholder="Enter the product description" value={description} onChange={e => setDescription(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="mb-4" controlId="formBasicPrice">
	        <Form.Label>Price:</Form.Label>
	        <Form.Control type="text" placeholder="Enter the product price" value={price} onChange={e => setPrice(e.target.value)}/>
	      </Form.Group>

	      {
	      	isActive ? 
		      	<Button className="mt-3" variant="primary" type="submit" id="submitBtn">Submit</Button>
		    :
		    	<Button className="mt-3" variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
	      }
	    </Form>
	    </>
	)
}
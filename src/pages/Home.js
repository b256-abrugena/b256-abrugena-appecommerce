import './Home.css';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

export default function Home() {
    const data = {
        title: "SKY PERFUMES",
        content: "The best and trusted online fragrance shop where you can buy branded and authentic perfumes at a very low price!!",
        destination: "/products",
        label: "View our Products!"
    }

    return (
        <>
            {/*S54: Banner component will receive the data coming from the "data" stated above.
                data (green) means that we are sending a data to banner component
                {data} means that the data (green) that we are sending is the "data" object that we created above.
            */}
            <Banner data={data}/>
            <Highlights />
        </>
    )
}

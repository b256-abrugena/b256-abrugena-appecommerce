import { Table, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function UpdateProduct(){

	const{ user, setUser } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [newName, setNewName] = useState('');
	const [newDescription, setNewDescription] = useState('');
	const [newPrice, setNewPrice] = useState('');

	const [isActiveButton, setIsActiveButton] = useState(false);

	console.log(newName);
	console.log(newDescription);
	console.log(newPrice);

	useEffect (() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

		if(newName !== '' && newDescription !== ''  && newPrice !==''){
			setIsActiveButton(true)
		}
		else{
			setIsActiveButton(false)
		}
	}) 


	function updateProduct(e){

		e.preventDefault();

		{
			(user.isAdmin) ?
			fetch(`${process.env.REACT_APP_API_URL}/products/update/${productId}`, {
				method: "PUT",
				headers:{
					'Content-Type': 'application/json',
					Authorization : `Bearer ${localStorage.getItem('token')}`,
				},
				body: JSON.stringify({
					name: newName,
					description: newDescription,
					price: newPrice,
					isAdmin: user.isAdmin
				})
			})
			.then(res => res.json())
			.then(data => {

				console.log(data)

				if(data !== false){

					Swal.fire({
						title: "Success!",
						icon: "success",
						text: "The product has been Updated!"
					})

					navigate("/admindashboard");

				}
				else{
					Swal.fire({
						title: "Error",
						icon: "error",
						text: "Check the details of the product and try again."
					})
				}

			})
			:
			Swal.fire({
				title: "Log-in Error",
				icon: "error",
				text: "Admin is not recognized."
			})

			navigate("/login");

		}

		setNewName('');
		setNewDescription('');
		setNewPrice('');
	}

	return(
		<>
			<h1 className="text-center mb-5 mt-5 pt-5 text-light display-2">Update Product Details</h1>
			<p className="h2 text-light text-center pb-3">You are about to update this product:</p>
			<Table className="table1 bg-dark text-light text-center mt-3 mb-3">
		      <thead>
		        <tr>
		          <th className="col-2">Product Name</th>
		          <th className="col-4">Description</th>
		          <th className="col-2">Price</th>
		        </tr>
		      </thead>
		      <tbody>
		        <tr>
		          <td className="col-2">{name}</td>
		          <td className="col-4">{description}</td>
		          <td className="col-2">₱ {price}</td>
		        </tr>
		      </tbody>
		    </Table>

		    <div className="bg-dark text-light text-center m-3 p-3">
		    	<h4 className="mt-3 mb-5 text-light">Fill-up the following details that must be updated through this form:</h4>
				<Form onSubmit={e => updateProduct(e)} className="mt-3 mb-3">
				  <Form.Group className="mb-4" controlId="formBasicNewName">
			        <Form.Label>Name:</Form.Label>
			        <Form.Control type="text" placeholder="Change Product Name" value={newName} onChange={e => setNewName(e.target.value)}/>
			      </Form.Group>

			      <Form.Group className="mb-4" controlId="formBasicNewDescription">
			        <Form.Label>Description:</Form.Label>
			        <Form.Control type="text" placeholder="Change Description" value={newDescription} onChange={e => setNewDescription(e.target.value)}/>
			      </Form.Group>

			      <Form.Group className="mb-4" controlId="formBasicNewPrice">
			        <Form.Label>Price:</Form.Label>
			        <Form.Control type="text" placeholder="Change the Price" value={newPrice} onChange={e => setNewPrice(e.target.value)}/>
			      </Form.Group>

			      {
			      	isActiveButton ? 
				      	<Button className="m-3" variant="primary" type="submit" id="submitBtn">Submit</Button>
				    :
				    	<Button className="m-3" variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
			      }
			      
			    </Form>
		    </div>
		    
	    </>
	)
}
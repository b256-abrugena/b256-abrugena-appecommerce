import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react'; 

import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';


export default function Login(){

	const{ user, setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	useEffect (() => {
		if(email !== '' && password !==''){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}) 

	const loginUser = (e) => {
		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/authentication`, {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(typeof data.access !== "undefined"){
				localStorage.setItem('token', data.access)

				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Log-in Success!",
					icon: "success",
					text: "Welcome to SKY Perfume Decants!"
				})
			}
			else{
				Swal.fire({
					title: "Log-in Failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}

		})

		setEmail('');
		setPassword('');

	}

	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})

		})
	}

	return(
		
		(user.id !== null && user.isAdmin) ?
			<Navigate to="/admindashboard" />
		:
		(user.id !== null) ?
			<Navigate to="/products" />
		:
		<Form onSubmit={e => loginUser(e)}>
		<h1 className="text-center mt-5 pt-5 mb-5 pt-5 text-light display-2">Log-in</h1>
		<p className="h2 text-light text-center pb-3">Please log-in your account so you can avail our awesome products and become fragrant!</p>
		<div className="bg-dark text-light text-center m-3 p-3">
	      <Form.Group className="m-3" controlId="formBasicEmail">
	        <Form.Label className="h5">E-mail address:</Form.Label>
	        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
	      </Form.Group>

	      <Form.Group className="m-3" controlId="formBasicPassword">
	        <Form.Label className="h5">Password:</Form.Label>
	        <Form.Control type="password" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)}/>
	      </Form.Group>
	    
	      {
	      	isActive ? 
		      	<Button className="m-3" variant="primary" type="submit" id="submitBtn">Log-in</Button>
		    :
		    	<Button className="m-3" variant="secondary" type="submit" id="submitBtn" disabled>Log-in</Button>
	      } 
	    </div> 
	    </Form>
	)
}
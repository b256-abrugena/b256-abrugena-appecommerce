import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';

import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function Register(){

	const{ user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [address, setAddress] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isActive, setIsActive] = useState(false);

	console.log(firstName);
	console.log(lastName);
	console.log(address);
	console.log(mobileNumber);
	console.log(email);
	console.log(password1);
	console.log(password2);


	useEffect (() => {
		if((firstName !== '' && lastName !== ''  && address !=='' && mobileNumber !== '' && email !== ''&& password1 !=='' && password2 !=='') && (password1 === password2)){
			setIsActive(true)
		}
		else{
			setIsActive(false)
		}
	}) 


	function registerUser(e){

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers:{
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				address: address,
				mobileNumber: mobileNumber,
				email: email,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {

			console.log(data)

			if(data !== false){

				Swal.fire({
					title: "Registration Success!",
					icon: "success",
					text: "Thank you so much for registering in SKY Perfume Decants! Let's proceed now to log-in!"
				})

				navigate("/login");

			}
			else{
				Swal.fire({
					title: "Registration Failed",
					icon: "error",
					text: "Check your registration details and try again."
				})
			}

		})

		setFirstName('');
		setLastName('');
		setAddress('');
		setMobileNumber('');
		setEmail('');
		setPassword1('');
		setPassword2('');
 
	}

	return(

		<Form onSubmit={e => registerUser(e)}>
		<h1 className="text-center mt-5 pt-5 mb-5 pt-5 text-light display-2">Register Now!</h1>
		<p className="h2 text-light text-center pb-3">By doing this, you will always be updated on our products, promos, and other exciting announcements from our online shop!</p>
		<div className="bg-dark text-light text-center m-3 p-3">
			  <Form.Group className="m-3" controlId="formBasicFirstName">
		        <Form.Label className="h5">First Name:</Form.Label>
		        <Form.Control type="text" placeholder="Enter your first name" value={firstName} onChange={e => setFirstName(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="m-3" controlId="formBasicLastName">
		        <Form.Label className="h5">Last Name:</Form.Label>
		        <Form.Control type="text" placeholder="Enter your last name" value={lastName} onChange={e => setLastName(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="m-3" controlId="formBasicAddress">
		        <Form.Label className="h5">Address:</Form.Label>
		        <Form.Control type="text" placeholder="Enter your address" value={address} onChange={e => setAddress(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="m-3" controlId="formBasicMobileNumber">
		        <Form.Label className="h5">Mobile Number:</Form.Label>
		        <Form.Control type="text" placeholder="Enter your mobile number" value={mobileNumber} onChange={e => setMobileNumber(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="m-3" controlId="formBasicEmail">
		        <Form.Label className="h5">Email address:</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" value={email} onChange={e => setEmail(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="m-3" controlId="formBasicPassword">
		        <Form.Label className="h5">Password:</Form.Label>
		        <Form.Control type="password" placeholder="Please enter your preferred password" value={password1} onChange={e => setPassword1(e.target.value)}/>
		      </Form.Group>

		      <Form.Group className="m-3" controlId="formBasicPassword2">
		        <Form.Label className="h5">Verify Password:</Form.Label>
		        <Form.Control type="password" placeholder="Please re-type your preferred password" value={password2} onChange={e => setPassword2(e.target.value)}/>
		      </Form.Group>

		      {
		      	isActive ? 
			      	<Button className="m-3" variant="primary" type="submit" id="submitBtn">Submit</Button>
			    :
			    	<Button className="m-3" variant="secondary" type="submit" id="submitBtn" disabled>Submit</Button>
		      }
		</div>
	    </Form>
	)
}
import { useState, useEffect, useContext } from 'react';
import { Form, Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link, useParams, useNavigate } from 'react-router-dom';
import UserContext from '../userContext';
import Swal from 'sweetalert2';

export default function ProductView() {

	const { user } = useContext(UserContext);

	const { productId } = useParams();

	const navigate = useNavigate();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);

	const [quantity, setQuantity] = useState("");
	const [totalAmount, setTotalAmount] = useState("");

	const [isActiveButton, setIsActiveButton] = useState(false);

	useEffect(() => {
		console.log(productId);

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);

		})

		if(quantity !== '' && totalAmount !== ''){
			setIsActiveButton(true)
		}
		else{
			setIsActiveButton(false)
		}
	})

	const order = (productId) => {
		fetch(`${process.env.REACT_APP_API_URL}/users/createorder`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}` 
			},
			body: JSON.stringify({

				productId: productId,
				productName: name,
				quantity: quantity,
				totalAmount : totalAmount
			
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true){
				Swal.fire({
					title: "Successfully Ordered!",
					icon: "success",
					text: "You have successfully ordered for this product."
				})

				navigate("/products");
			}
			else{
				Swal.fire({
					title: "Something went wrong!",
					icon: "error",
					text: "Please try again"
				})
			}

		})

		setQuantity('');
		setTotalAmount('');
	}

	return(
		<Container className="mt-5">
			<Row>
				<Col lg={{ span: 8, offset: 2 }}>
					<h1 className="text-center mb-5 pt-5 text-light display-2">Product Check-out</h1>
					<p className="h2 text-light text-center pb-3">You are about to order this product:</p>
					<Card className="bg-dark pcard p-3 mb-3 d-md-flex justify-content-around">
						<Card.Body className="text-center">
							<Card.Title className="text-center bg-light text-dark pt-1 pb-1">{name}</Card.Title>
							<Card.Subtitle className="text-light mt-4">Description:</Card.Subtitle>
							<Card.Text className="text-light mb-4">{description}</Card.Text>
							<Card.Subtitle className="text-light">Price:</Card.Subtitle>
							<Card.Text className="text-light">₱ {price}</Card.Text>
						</Card.Body>		
					</Card>

					<Form onSubmit={productId => order(productId)}>
					  <div className="bg-dark text-light text-center m-3 p-3">
					  	  <Form.Group className="mb-3" controlId="formBasicQuantity">
					        <Form.Label>Quantity:</Form.Label>
					        <Form.Control type="number" placeholder="Enter the quantity you want to order for this perfume" value={quantity} onChange={productId => setQuantity(productId.target.value)}/>
					      </Form.Group>

					      <Form.Group className="mb-3" controlId="formBasicTotalAmount">
					        <Form.Label>Total Amount (in ₱):</Form.Label>
					        <Form.Control type="number" placeholder="Enter the total amount" value={totalAmount} onChange={productId => setTotalAmount(productId.target.value)} />
					      </Form.Group>

							{
								
								(user.id !== null)?
								<>
									{
										(user.isAdmin || !isActiveButton) ?
										<Button className="mt-3" variant="secondary" disabled>Order Now!</Button>
										:
										<Button className="mt-3" variant="primary" block onClick={() => order(productId)}>Order Now!</Button>
									}
								</>
								:
								<Button className="mt-3" variant="danger" text-light as={Link} to="/login">Login</Button>
							}
					  </div>
				    </Form>
				</Col>
			</Row>
		</Container>
	)
}
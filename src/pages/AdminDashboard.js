import { useState, useEffect } from 'react';
import Banner from '../components/Banner';
import ProductTable from '../components/ProductTable';

export default function AdminDashboard(){
	
	const data = {
        title: "Admin Dashboard",
        content: "Welcome Admin! You may now create, update, or archive our products.",
        destination: "/newproduct",
        label: "Add New Product"
    }

    const[ products, setProducts ] = useState([])

    useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(res => res.json())
		.then(data => {

			setProducts(data.map(product => {
				return(
					<ProductTable key = {product.id} productProp={product} />
				)
			}))
		})
	})


	return(
		<>
			<Banner data={data}/>
			{products}
		</>
	)
}
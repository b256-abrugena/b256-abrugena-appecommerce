import Banner from '../components/Banner';
import Highlights from '../components/Highlights';
import { useState, useEffect, useContext } from 'react'; 
import { Navigate, Link, useParams, useNavigate } from 'react-router-dom';
import img44 from '../images/img44.jpg'

export default function ContactUs(){
	const data = {
        title: "Contact Us",
        content: "Do you have any questions or concerns in line with our products? Feel free to reach us, and we will answer it as soon as we can. Thank you so much, and have a fragrant day!",
        destination: "/",
        label: "Back to Home"
    }

	return(
		<>
            <Banner data={data}/>
            <div className="container-fluid align-middle">
	            <div className="row">
	                <div className="caption col-sm-6 order-2 order-md-1 p-5 align-middle">
	                    
	                    <h4 className="text-center pt-2 pb-2 display-2">SKY PERFUMES</h4>
	                    
	                    <div className="pt-5 pb-2">
	                    	<h3 className="text-center">Owner:</h3>
	                    	<p className="p-2 text-center">Kang Haneul (강하늘)</p>
	                    </div>

	                    <div className="pt-2 pb-2">
	                    	<h3 className="text-center">E-mail Address:</h3>
	                    	<p className="p-2 text-center">talk@skyperfumes.com</p>
	                    </div>

	                    <div className="pt-2 pb-2">
	                    	<h3 className="text-center">Contact Number:</h3>
	                    	<p className="p-2 text-center">(+63)912-3456789</p>
	                    </div>

	                    <div className="pt-2 pb-2">
	                    	<h3 className="text-center">Facebook Page:</h3>
	                    	<p className="p-2 text-center">fb.com/skyperfumesofficial</p>
	                    </div>

	                    <div className="pt-2 pb-2">
	                    	<h3 className="text-center">Instagram:</h3>
	                    	<p className="p-2 text-center">@skyperfumesofficial</p>
	                    </div>
	                </div>

	                <div className="col-sm-6 order-1 order-md-2">
	                    <img src={img44} className="align-middle img-fluid pl-2 pr-2"></img>
	                </div>
	            </div>
        	</div>
        </>	
	)
}
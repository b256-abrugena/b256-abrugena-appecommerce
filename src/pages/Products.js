import { useState, useEffect } from 'react';
import { Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import Slideshow from '../components/Slideshow'; 

export default function Products(){

	const[ products, setProducts ] = useState([])

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/activeproducts`)
		.then(res => res.json())
		.then(data => {

			setProducts(data.map(product => {
				return(
					<>
						<ProductCard key = {product.id} productProp={product} />
					</>
				)
			}))
		})
	})

	return (
		<>
			<h1 className="text-center mt-5 pt-5 mb-5 pt-5 text-light display-2">Our Products</h1>
			<div className="text-center pb-5">
				<p className="text-light h4 pb-2">To view the images of our products, you may browse our gallery below:</p>
				<Slideshow />
			</div>
			{products}
		</>
	)

}
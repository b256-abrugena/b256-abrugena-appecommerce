import {Row, Col, Card} from 'react-bootstrap';
import './Highlights.css'
import img01 from '../images/kanghaneulperfume.jpg';

export default function Highlights(){
	return (
		<div className="container-fluid pt-4 pb-4">
            <div className="row">
                <div className="caption col-sm-6 order-2 order-md-1 p-5 align-middle">
                    <h4 className="text-center pt-3 pb-3 display-5">About Shop</h4>
                    <h3 className="text-center pt-3">Annyeonghaseyo everyone! 🫰</h3>
                    <h3 className="text-center pb-3">Thank you so much for visiting my online shop! 😄</h3>
                    <p className="p-2 text-center">In my personal life, I absolutely love fragrances! Ever since I was a kid, I have found people who wear perfume attractive. It gave me the impression that every time you wear an outfit paired with a nice perfume, you will be more presentable and stand out among the crowd! Not only that, it will also boost your confidence, and people will like you more when you smell good. </p>
                    <p className="p-2 text-center"> As an actor, I make sure to myself that I stay fragrant during my tapings and other events. And now, I would like to share this hobby and to help people who want to buy perfumes without spending too much money!</p>
                    <p className="text-center"><em>-- Kang Haneul (강하늘) --</em></p>
                    <p className="text-center"><em>Owner, Sky Perfumes</em></p>
                </div>

                <div className="col-sm-6 order-1 order-md-2">
                    <img src={img01} className="img-fluid pl-2 pr-2"></img>
                </div>
            </div>
        </div>
	)
}
import { Carousel } from 'react-bootstrap';
import './Slideshow.css';
import img02 from '../images/img02.jpg';
import img03 from '../images/img03.jpg';
import img04 from '../images/img04.jpg';
import img05 from '../images/img05.jpg';
import img06 from '../images/img06.jpg';
import img07 from '../images/img07.jpg';
import img08 from '../images/img08.jpg';
import img09 from '../images/img09.jpg';
import img10 from '../images/img10.jpg';
import img11 from '../images/img11.jpg';
import img12 from '../images/img12.jpg';
import img13 from '../images/img13.jpg';
import img14 from '../images/img14.jpg';
import img15 from '../images/img15.jpg';
import img16 from '../images/img16.jpg';
import img17 from '../images/img17.jpg';
import img18 from '../images/img18.jpg';
import img19 from '../images/img19.jpg';
import img20 from '../images/img20.jpg';
import img21 from '../images/img21.jpg';
import img22 from '../images/img22.jpg';
import img23 from '../images/img23.jpg';
import img24 from '../images/img24.jpg';
import img25 from '../images/img25.jpg';
import img26 from '../images/img26.jpg';
import img27 from '../images/img27.jpg';
import img28 from '../images/img28.jpg';
import img29 from '../images/img29.jpg';
import img30 from '../images/img30.jpg';
import img31 from '../images/img31.jpg';
import img32 from '../images/img32.jpg';
import img33 from '../images/img33.jpg';
import img34 from '../images/img34.jpg';
import img35 from '../images/img35.jpg';
import img36 from '../images/img36.jpg';
import img37 from '../images/img37.jpg';
import img38 from '../images/img38.jpg';
import img39 from '../images/img39.jpg';
import img40 from '../images/img40.jpg';

export default function Slideshow (){
	return(
		<Carousel className="pb-3">
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img02}
	          alt="Chanel Allure Homme Sport Eau Extreme"
	        />
	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Chanel Allure Homme Sport Eau Extreme</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img03}
	          alt="Bleu de Chanel Eau de Parfum"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Bleu de Chanel Eau de Parfum</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img04}
	          alt="Dior Sauvage Eau de Toilette"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Dior Sauvage Eau de Toilette</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img05}
	          alt="Club de Nuit Intense for Men"
	        />

	       <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Club de Nuit Intense for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img06}
	          alt="Dolce & Gabbana Light Blue Intense for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Dolce & Gabbana Light Blue Intense for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img07}
	          alt="Nautica Voyage for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Nautica Voyage for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img08}
	          alt="Versace Eros Eau de Toilette"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Versace Eros Eau de Toilette</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img09}
	          alt="Acqua di Gio Profumo for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Acqua di Gio Profumo for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img10}
	          alt="YSL Y Eau de Parfum"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>YSL Y Eau de Parfum</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img11}
	          alt="Spicebomb Extreme for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Spicebomb Extreme for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img12}
	          alt="Hawas for Him"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Hawas for Him</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img13}
	          alt="Mancera Cedrat Boise"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Mancera Cedrat Boise</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img14}
	          alt="Zara Vibrant Leather EDP"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Zara Vibrant Leather EDP</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img15}
	          alt="Paco Rabanne Invictus Aqua for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Paco Rabanne Invictus Aqua for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img16}
	          alt="Jean Paul Gaultier Ultra Male"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Jean Paul Gaultier Ultra Male</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img17}
	          alt="Clinique Happy for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Clinique Happy for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img18}
	          alt="Antonio Banderas Blue Seduction"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Antonio Banderas Blue Seduction</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img19}
	          alt="Polo Deep Blue Parfum"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Polo Deep Blue Parfum</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img20}
	          alt="Versace Dylan Blue"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Versace Dylan Blue</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img21}
	          alt="Coach for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Coach for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img22}
	          alt="Montblanc Explorer"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Montblanc Explorer</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img23}
	          alt="Versace Pour Homme"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Versace Pour Homme</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img24}
	          alt="Lacoste Blanc L.12.12 for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Versace Pour Homme</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img25}
	          alt="Prada Luna Rossa Carbon"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Prada Luna Rossa Carbon</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img26}
	          alt="Afnan 9 PM for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Afnan 9 PM for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img27}
	          alt="Jimmy Choo Man Ice"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Jimmy Choo Man Ice</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img28}
	          alt="Terre d'Hermes for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Terre d'Hermes for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img29}
	          alt="Lacoste L'Homme Intense"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Lacoste L'Homme Intense</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img30}
	          alt="Luxodor Loyal Agar for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Luxodor Loyal Agar for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img31}
	          alt="Calvin Klein One Shock for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Calvin Klein One Shock for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img32}
	          alt="Chanel Allure Homme Edition Blanche"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Chanel Allure Homme Edition Blanche</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img33}
	          alt="Luxodor Prince for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Luxodor Prince for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img34}
	          alt="Lanvin Eclat d'Arpege for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Lanvin Eclat d'Arpege for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img35}
	          alt="Bvlgari Aqua Pour Homme Atlantique"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Bvlgari Aqua Pour Homme Atlantique</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img36}
	          alt="Club de Nuit Milestone"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Club de Nuit Milestone</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img37}
	          alt="YSL Kouros for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>YSL Kouros for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img38}
	          alt="Prada L'Homme for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Prada L'Homme for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img39}
	          alt="Prada L'Homme Intense for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Prada L'Homme Intense for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	      <Carousel.Item>
	        <img
	          className="d-inline-block w-35 text-center"
	          src={img40}
	          alt="Acqua di Gio Profondo for Men"
	        />

	        <Carousel.Caption className="cap bg-dark text-light">
	          <h4>Acqua di Gio Profondo for Men</h4>
	        </Carousel.Caption>
	      </Carousel.Item>
	    </Carousel>
	)
}

import { Container, Navbar, Nav } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../userContext';
import { useContext } from 'react';

export default function AppNavBar(){

	const { user } = useContext(UserContext);

	return(
		<Navbar bg="dark" className="navbar-expand-lg navbar-dark fixed-top text-center">
	      <Container>
	        <Navbar.Brand className="text-light" as={Link} to="/"><strong>SKY PERFUMES</strong></Navbar.Brand>
	        <Navbar.Toggle aria-controls="basic-navbar-nav"/>
	        <Navbar.Collapse id="basic-navbar-nav">
	          <Nav className="me-auto">
	            {
	            	(user.isAdmin) ?
	            		<Nav.Link as={NavLink} to="/admindashboard">Dashboard</Nav.Link>
	            		:
	            		<Nav.Link as={NavLink} to="/products">Products</Nav.Link>
	            }
	            <Nav.Link as={NavLink} to="/reviews">Reviews</Nav.Link>
	            {
	            	(user.id !== null) ?
	            		<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
	            	:
	            	// Fragments <> and </>
    				// Fragments are needed when there are two or more components, pages or html elements present in the code.
    				// Login and Register were created last S53 Discussion 
	            	<>
	            		<Nav.Link as={NavLink} to="/login">Login</Nav.Link> 
	            		<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
	            	</>
	            }
	            <Nav.Link as={NavLink} to="/contactus">Contact Us</Nav.Link>
	          </Nav>
	        </Navbar.Collapse>
	      </Container>
	    </Navbar>
	)
}
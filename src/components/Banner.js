import { Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './Banner.css'

export default function Banner({data}) {

    const {title, content, destination, label} = data;

	return(
		<Row className="text-light pt-5 pb-3">
			<Col className="p-5 text-center">
				<h1 className="pt-3 pb-4 display-2">{title}</h1>
				<h2 className="cont pt-2 pb-5"><em>{content}</em></h2>
				<Button as={Link} to={destination} variant="dark" className="btn-lg">{label}</Button>
			</Col>
		</Row>
	)
}

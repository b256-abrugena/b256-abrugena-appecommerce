import './ProductCard.css'
import { Link } from 'react-router-dom';
import { Button, Card } from 'react-bootstrap';

export default function ProductCard ({productProp}){

	const { name, description, price, _id } = productProp;

	return(

		<Card className="bg-dark pcard p-3 mb-3 d-md-flex justify-content-around">
	      <Card.Body>
	        <Card.Title className="text-center bg-light text-dark pt-1 pb-1">{name}</Card.Title>
	        <Card.Subtitle className="text-light mt-4">Description:</Card.Subtitle>
	        <Card.Text className="text-light mb-4">{description}</Card.Text>
	        <Card.Subtitle className="text-light">Price:</Card.Subtitle>
	        <Card.Text className="text-light">₱ {price}</Card.Text>
	        <div className="text-center">
	        	<Button variant="primary" className="btn-lg text-light" as={Link} to={`/productView/${_id}`}>Order Now!</Button>
	        </div>
	      </Card.Body>
	    </Card>
	)

}
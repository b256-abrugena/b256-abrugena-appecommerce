import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function ProductTable ({productProp}){

	const { name, description, price, _id, isActive } = productProp;

	return(
		<Table className="table1 bg-dark text-light text-center mt-3 mb-3">
	      <thead>
	        <tr>
	          <th className="col-2">Product Name</th>
	          <th className="col-3">Description</th>
	          <th className="col-1">Price</th>
	          <th className="col-1">Availability</th>
	          <th className="col-1">Actions</th>
	        </tr>
	      </thead>
	      <tbody>
	        <tr>
	          <td className="col-2 align-middle">{name}</td>
	          <td className="col-3 align-middle">{description}</td>
	          <td className="col-1 align-middle">₱ {price}</td>
	          {
	          	(isActive) ?
	          	<td className="col-1 align-middle">Available</td>
	          	:
	          	<td className="col-1 align-middle">Not Available</td>
	          }
	          <td className="col-1">
	          	<Button className="mt-1 mb-1" variant="warning" text-dark as={Link} to={`/productView/${_id}`}>Product View</Button>
	          	<Button className="mt-1 mb-1" variant="primary" text-light as={Link} to={`/update/${_id}`}>Update Product</Button>
	          	{
	          		(isActive) ?
	          		<Button className="mt-1 mb-1" variant="danger" text-light as={Link} to={`/archive/${_id}`}>Archive Product</Button>
	          		:
	          		<Button className="mt-1 mb-1" variant="success" text-light as={Link} to={`/activate/${_id}`}>Activate Product</Button>
	          	}
	          </td>
	        </tr>
	      </tbody>
	    </Table>
	)

}